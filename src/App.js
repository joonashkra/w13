import './App.css';

function App() {
  //here is an exmple how to create an array
  const weekDays = ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"];
  //here is an example how to use the array above to create an array of option elements
  //see https://www.w3schools.com/jsref/jsref_map.asp

  return (
    <div className="App">
      <header className='App-header'>W13 weekdays in table</header>
      <table>
        <thead>
          <tr><th>Weekday</th></tr>
        </thead>
        <tbody>
          {weekDays.map((weekDay) => 
          <tr>
            <td>{weekDay}</td>
          </tr>
          )}
        </tbody>
      </table>
    </div>
  );
}

export default App;
